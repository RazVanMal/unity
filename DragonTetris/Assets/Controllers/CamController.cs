﻿using System;
using UnityEngine;

public class CamController : MonoBehaviour
{
    [SerializeField] float sensitivity = 0.25f;
    
    
    
    [SerializeField] GameObject grid;
    private Vector3 gridSize;

    private Transform focalPoint;   // Lateral movement // target Y
    private Transform rotationObj;  // rotTarget XZ

    private Vector3 lastPos;

    private void Awake()
    {
        rotationObj = transform.parent;
        focalPoint = rotationObj.transform.parent;

        gridSize = grid.GetComponent<GridController>().GetGridSize();

        InitCam();
    }
    

    void Update()
    {
        transform.LookAt(focalPoint);

        if (Input.GetMouseButtonDown(0))
        {
            lastPos = Input.mousePosition;
        }
        if (Input.GetMouseButtonDown(1))
        {
            lastPos = Input.mousePosition;
        }
       
        RotateCam();
        MoveCam();
        UpZoomdate();
    }


    private void MoveCam()
    {
        if (Input.GetMouseButton(1))
        {
            Vector3 deltaPos = Input.mousePosition - lastPos;

            Vector3 pos = Camera.main.ScreenToViewportPoint(deltaPos);
            Vector3 move = pos * 20;

            transform.Translate(move);

            lastPos = Input.mousePosition;

        }
    }

    private void RotateCam()
    {
        if (Input.GetMouseButton(0))
        {

            Debug.Log("Mouse - 0");
            Vector3 deltaPos = Input.mousePosition - lastPos;
            float angleY = -deltaPos.y * sensitivity;
            float angleX = deltaPos.x * sensitivity;

            handleAxis_Clamped(angleY);     // X - axis - up and down (clamped)
            handleAxis_Unlimited(angleX);   // Y - axis - left and right (unlimited)

            lastPos = Input.mousePosition;
        }
    }

    void UpZoomdate()
    {
        // -------------------Code for Zooming Out------------
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (Camera.main.fieldOfView <= 125)
                Camera.main.fieldOfView += 2;
            if (Camera.main.orthographicSize <= 20)
                Camera.main.orthographicSize += 0.5f;

        }
        // ---------------Code for Zooming In------------------------
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (Camera.main.fieldOfView > 2)
                Camera.main.fieldOfView -= 2;
            if (Camera.main.orthographicSize >= 1)
                Camera.main.orthographicSize -= 0.5f;
        }

        // -------Code to switch camera between Perspective and Orthographic--------
        if (Input.GetKeyUp(KeyCode.B))
        {
            if (Camera.main.orthographic == true)
                Camera.main.orthographic = false;
            else
                Camera.main.orthographic = true;
        }
    }

    private void handleAxis_Clamped(float axis)
    {
        #region -=*=- Bugs and Clamps -=*=- 
        /* 
         * If we rotate too high, over the "edge", the compiler doesnt know what to do,
         *  so we need to limit the up-down range => clamp
         * Since we cannot clamp negative values, we need a helper function.
         * 
         * If we are at point 0, we want 90 degrees to each side, 
         *  Thats [-90°,90°], which means 180°,  so.. The clamp should be 180 up-to-down. 
         *  
         *  We'll make it 85°. Just to be on the safe side
        */
        #endregion

        Vector3 angles = rotationObj.transform.eulerAngles;
        angles.x += axis;
        angles.x = clampAngle(angles.x,-85, 85);
        rotationObj.transform.eulerAngles = angles;
    }

    private void handleAxis_Unlimited(float axis)
    {
        focalPoint.RotateAround(focalPoint.position, Vector3.up, axis);
    }

    float clampAngle(float angle, float from, float to)
    {
        if (angle < 0) { angle = 360 + angle; }
        if (angle > 180) { return Mathf.Max(angle, 360 + from); }

        return Mathf.Min(angle, to);

    }

    private void InitCam()
    {
        focalPoint.transform.position = gridSize * 0.5f;

        // Ortho Setup
        Camera.main.orthographicSize = Mathf.Max(gridSize.x, gridSize.z) * 1.5f;
        rotationObj.transform.rotation = Quaternion.Euler(22.5f, 45f, 0f);

        // Perspective Setup
        transform.position = new Vector3(-gridSize.x, gridSize.y, -gridSize.z);

        Camera.main.orthographic = true;
    }

}







 
   
 
   

