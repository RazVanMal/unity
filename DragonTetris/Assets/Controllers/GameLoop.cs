﻿

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoop : MonoBehaviour
{

    #region Vars

    // Bricks
    [SerializeField] BrickFactory brickFactory;
    [SerializeField] GameObject[] bricksArray;

    List<GameObject> noviceBricks = new List<GameObject>();
    List<GameObject> adventurerBricks = new List<GameObject>();
    List<GameObject> heroBricks = new List<GameObject>();

    GameObject activeBrick;
    GameObject ghostBrick;
    GameObject previewBrick;

    // Preview
    [SerializeField] GameObject previewPoint;
    [SerializeField] float previewRotationSpeed = 100f;

    //Score //#TODO - move to UIHandler
    [SerializeField] TextMeshProUGUI scoreGui;

    // Grid
    [SerializeField] GridController grid;
    Vector3Int gridSize;

    // Ghost bricks
    [SerializeField] Material ghostMaterial;
    [SerializeField] float ghostAlpha = 0.20f;

    // Active bricks
    int activeRandomIndex = 0;

    // FallSpeed
    [SerializeField] float fallSpeed = 1f;
    float savedSpeed = 1f;    
    float prevTime = 0f;

    // Game over :-(
    bool gameOver = false;

    AudioSource audioSource;

    #endregion Vars

    #region LifeCycle

    void Start()
    {

        Debug.Log("GameLoop - Start");
        gameOver = false;

        DataHandler.instance.score = 0;
        scoreGui.text = "Score : "+DataHandler.instance.score.ToString("D3");
        Debug.Log("    - Set Score : " + DataHandler.instance.score);

        fallSpeed = DataHandler.instance.gameSpeed;
        Debug.Log("    - Set Speed : " + DataHandler.instance.gameSpeed);

        gridSize = grid.GetGridSize();
        Debug.Log("    - Grid Size : " + gridSize);


        GameObject[] tempArray = brickFactory.GetBrickArray();
        
        foreach (var brick in tempArray)
        {
             if (brick.name.Equals("C_Brick")) { AddNovice(brick); }
            else if (brick.name.Equals("D_Brick")) { AddHero(brick); }
            else if (brick.name.Equals("G_Brick")) { AddAdvnt(brick); }
            else if (brick.name.Equals("I_Brick")) { AddNovice(brick); }
            else if (brick.name.Equals("J_Brick")) { AddAdvnt(brick); }
            else if (brick.name.Equals("L_Brick")) { AddNovice(brick); }
            else if (brick.name.Equals("O_Brick")) { AddAdvnt(brick); }
            else if (brick.name.Equals("OO_Brick")){ AddNovice(brick); }
            else if (brick.name.Equals("P_Brick")) { AddHero(brick); }
            else if (brick.name.Equals("Q_Brick")) { AddHero(brick); }
            else if (brick.name.Equals("S_Brick")) { AddNovice(brick); }
            else if (brick.name.Equals("T_Brick")) { AddNovice(brick); }
        }


        //Debug.Log("****************************************************");      
        //Debug.Log("      - Arr  NOV = " + noviceBricks.Count);
        //Debug.Log("      - Arr  DEV = " + adventurerBricks.Count);
        //Debug.Log("      - Arr  PRO = " + heroBricks.Count);
        //Debug.Log("****************************************************");


        switch (DataHandler.instance.difficulty)
        {
            case 1: { bricksArray = noviceBricks.ToArray(); } break;
            case 2: { bricksArray = adventurerBricks.ToArray(); } break;
            case 3: { bricksArray = heroBricks.ToArray(); } break;
        }
        
        Debug.Log("    - Difficulty: " + DataHandler.instance.difficulty);
        Debug.Log("      - Arr Length = " + bricksArray.Length);

        savedSpeed = fallSpeed;

        audioSource = GetComponent<AudioSource>();

        CalculateNextRandomIndex();
        SpawnNewBrick();
    }

    private void AddNovice(GameObject brick)
    {
        Debug.Log("Added " + brick.name + "to nov + adv + hero");
        noviceBricks.Add(brick);
        adventurerBricks.Add(brick);
        heroBricks.Add(brick);
    }

    private void AddAdvnt(GameObject brick)
    {
        Debug.Log("Added " + brick.name + "to adv + hero");
        adventurerBricks.Add(brick);
        heroBricks.Add(brick);
    }

    private void AddHero(GameObject brick)
    {
        Debug.Log("Added " + brick.name + "to hero");
        heroBricks.Add(brick);
    }

    void Update()
    {
        if (gameOver){
            //remove last bricks,
            if (activeBrick != null) { Destroy(activeBrick); }
            if (ghostBrick!= null) { Destroy(ghostBrick); }
            if (previewBrick != null) { Destroy(previewBrick); }
            
            StartCoroutine(fillGrid(trigerEndGame));
            return;
        }

        if(previewBrick != null)
        {
            previewBrick.transform.RotateAround(previewBrick.transform.position, Vector3.up, previewRotationSpeed * Time.deltaTime);
        }

        if (Time.time - prevTime > fallSpeed)
        {
            activeBrick.transform.position += Vector3.down;

            if (IsValidMove(activeBrick))
            {
                grid.UpdateGameBoardCube(activeBrick);   
            }
            else
            {
                if (isGameOverCheck(activeBrick)) 
                {
                    StartCoroutine(fillGrid(trigerEndGame));
                    return;
                }
                else
                {
                    activeBrick.transform.position += Vector3.up;
                    HandleRemoveLayer();// Delete layer, if posible
                    SpawnNewBrick();
                }
            }
            prevTime = Time.time;
        }
        SortInput();
    }

    #endregion LifeCycle

    #region Input

    private void SortInput()
    {
        if (Input.GetKeyDown(KeyCode.W)) { MoveLateralTo(Vector3.forward); }
        if (Input.GetKeyDown(KeyCode.A)) { MoveLateralTo(Vector3.left); }
        if (Input.GetKeyDown(KeyCode.S)) { MoveLateralTo(Vector3.back); }
        if (Input.GetKeyDown(KeyCode.D)) { MoveLateralTo(Vector3.right); }

        if (Input.GetKeyDown(KeyCode.Z)) { RotateTo(ConstClass.X_Rotation); }
        if (Input.GetKeyDown(KeyCode.X)) { RotateTo(ConstClass.Y_Rotation); }
        if (Input.GetKeyDown(KeyCode.C)) { RotateTo(ConstClass.Z_Rotation); }

        if (Input.GetKeyDown(KeyCode.Space)) { DropFast(); }

        if (Input.GetKeyDown(KeyCode.M)) { audioSource.mute = !audioSource.mute; }
        if (Input.GetKeyDown(KeyCode.Minus)) { audioSource.mute = !audioSource.mute; }
    }

    public void MoveLateralTo(Vector3 direction)
    {
        activeBrick.transform.position += direction;

        if (IsValidMove(activeBrick))
        {
            grid.UpdateGameBoardCube(activeBrick);
        }
        else
        {
            activeBrick.transform.position -= direction;
        }
            DropGhostBrick();
    }

    public void RotateTo(Vector3 rotation)
    {
        activeBrick.transform.Rotate(rotation, Space.World);
        if (IsValidMove(activeBrick))
        {
            grid.UpdateGameBoardCube(activeBrick);
        }
        else
        {
            activeBrick.transform.Rotate(-rotation, Space.World);
        }
            DropGhostBrick();
    }

    public void DropFast()
    {

        fallSpeed = 0.01f;
    }

    #endregion Input

    #region spawnBricks

    private void SpawnNewBrick()
    {
        //reset some params..
        fallSpeed = savedSpeed;
        Vector3 spawnPoint = new Vector3(gridSize.x / 2, gridSize.y, gridSize.z / 2);

        spawnActiveBrick(spawnPoint);
        spawnGhostBrick(spawnPoint);

        CalculateNextRandomIndex();

        spawnPreviewBrick(previewPoint.transform.position);
    }

    private void spawnPreviewBrick(Vector3 spawnPoint)
    {
        if (previewBrick != null) { Destroy(previewBrick); }
        previewBrick = Instantiate(bricksArray[activeRandomIndex], spawnPoint, Quaternion.identity) as GameObject;
        previewBrick.SetActive(true);
        SetupPreviewBrick(previewBrick);
    }

    private void spawnGhostBrick(Vector3 spawnPoint)
    {
        if (ghostBrick != null) { Destroy(ghostBrick); }
        ghostBrick = Instantiate(bricksArray[activeRandomIndex], spawnPoint, Quaternion.identity) as GameObject;
        ghostBrick.SetActive(true);

        GhostTheBrick(ghostBrick);
        DropGhostBrick();
    }

    private void spawnActiveBrick(Vector3 spawnPoint)
    {
        
        activeBrick = Instantiate(bricksArray[activeRandomIndex], spawnPoint, Quaternion.identity) as GameObject;
        activeBrick.SetActive(true);

        if (!IsValidMove(activeBrick))
        {
            gameOver = true;
            StartCoroutine(fillGrid(trigerEndGame));
        }
    }

    IEnumerator fillGrid(Action callback)
    {
        for (int y = 0; y < gridSize.y; y++)
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                for (int x = 0; x < gridSize.x; x++)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                    cube.transform.position = new Vector3(x, y, z);
                    cube.GetComponent<Renderer>().material.color = Color.gray;
                    grid.UpdateGameBoardCube(cube);
                }
                    yield return new WaitForSeconds(0.01f);
            }
        }
        if (callback != null) callback();
    }

    void trigerEndGame()
    {
        DataHandler.instance.UpdateHighScore();
        int lastScene = SceneManager.sceneCountInBuildSettings;
        SceneManager.LoadScene(lastScene-1);
    }

    private void GhostTheBrick(GameObject ghostBrick)
    {
        string[] splitArray = ghostBrick.name.Split(char.Parse("_"));
        ghostBrick.name = splitArray[0]+"_Ghost";

        foreach (Transform cube in ghostBrick.transform)
        {
            Renderer rend = cube.GetComponent<Renderer>();

            Color newColor = rend.material.color;
            newColor.a = ghostAlpha;

            Material mat = new Material(ghostMaterial);

            mat.color = newColor;
            rend.material = mat;
        }
    }

    private void SetupPreviewBrick(GameObject previewBrick)
    {
        string[] splitArray = previewBrick.name.Split(char.Parse("_"));
        previewBrick.name = splitArray[0] + "_Preview";
    }

    private void DropGhostBrick()
    {
        //copy position
        ghostBrick.transform.rotation = activeBrick.transform.rotation;
        ghostBrick.transform.position = activeBrick.transform.position;

        //yeet down
        while (IsValidMove(ghostBrick, true))
        {
            ghostBrick.transform.position += Vector3.down;
        }
        if (!IsValidMove(ghostBrick, true))
        {
            ghostBrick.transform.position += Vector3.up;
        }
    }

    private void CalculateNextRandomIndex()
    {
        activeRandomIndex = UnityEngine.Random.Range(0, bricksArray.Length);
    }


    #endregion spawnBricks

    #region movementChecks

    bool isGameOverCheck(GameObject theBrick)
    {
        if (gameOver) 
        {
            StartCoroutine(fillGrid(trigerEndGame));
            return true;
        }

        foreach (Transform cube in theBrick.transform)
        {
            Vector3 pos = Utils.Round(cube.position);
            if(pos.y > gridSize.y)
            {
                gameOver = true;
                StartCoroutine(fillGrid(trigerEndGame));
                return true;
            }
        }
        return false;
    }

    bool IsValidMove(GameObject theBrick, bool ghostCheck = false, bool Verbos = true)
    {
        foreach (Transform cube in theBrick.transform)
        {
            Vector3 pos = Utils.Round(cube.position);

            // Is in grid boundries?
            if (!IsInsideGrid(pos)) {return false; }

            // Is there something there? (that is NOT "us")
            Transform t = grid.GetCubeAtPos(pos);

            if (t != null)
            {
                if (ghostCheck)
                {
                    // We are over ourselves..
                    if (t.parent == activeBrick.transform) {return true;}
                }

                if (t.parent != theBrick.transform) {return false;}
            }
        }
        return true;
    }

    public bool IsInsideGrid(Vector3 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < gridSize.x &&
                (int)pos.z >= 0 && (int)pos.z < gridSize.z &&
                (int)pos.y >= 0);
    }

    #endregion movementChecks

    #region DeleteLayer

    public void HandleRemoveLayer()
    {
        int layersCleared = 0;

        for (int y = gridSize.y - 1; y >= 0; y--)
        {
            // CHECK if layer is FULL LAYER
            if (IsLayerFull(y))
            {
                layersCleared++;
                
                DeleteLayerAt(y);
                MoveAllLayerDown(y);
                HandleScore();
            }
        }

    }

    private bool IsLayerFull(int y)
    {
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                if (grid.gameBoardCube[x, y, z] == null)
                {
                    return false;
                }
            }
        }
        return true;
    }

    private void DeleteLayerAt(int y)
    {
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                Destroy(grid.gameBoardCube[x, y, z].gameObject);
                grid.gameBoardCube[x, y, z] = null;
            }
        }
    }

    private void MoveAllLayerDown(int y)
    {
        for (int i = y; i < gridSize.y; i++)
        {
            MoveOneLayerDown(i);
        }
    }

    private void MoveOneLayerDown(int y)
    {
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                if (grid.gameBoardCube[x, y, z] != null)
                {
                    grid.gameBoardCube[x, y - 1, z] = grid.gameBoardCube[x, y, z];
                    grid.gameBoardCube[x, y, z] = null;
                    grid.gameBoardCube[x, y - 1, z].position += Vector3.down;
                }

            }
        }
    }

    private void HandleScore()
    {

        DataHandler.instance.score++;
        DataHandler.instance.SaveData();

        scoreGui.text = "Score : " + DataHandler.instance.score.ToString("D3");

        fallSpeed = (DataHandler.instance.score % 10);
    }

    #endregion DeleteLayer

}
