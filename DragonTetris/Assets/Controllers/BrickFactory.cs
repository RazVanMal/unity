﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BrickFactory : MonoBehaviour
{

    [SerializeField] GameObject brickEditor;

    // All Serialized for Debug purposes.
    [SerializeField] BrickContainer container = new BrickContainer();
    public List<GameObject> bakedBricks = new List<GameObject>();
    [SerializeField] bool debugShowBricks= false;


    void Awake()
    {

        if (File.Exists(Application.persistentDataPath + ConstClass.BrickJsonFileName))
        {
            BrickToData brickEditor = new BrickToData();
            container = LoadBricksFromFile();
        }
        else
        {
            brickEditor.GetComponent<BrickToData>().CreateBricks();
        }


        CreateCubes(container);

        if (debugShowBricks)
        {
            SetBricksInCircle(bakedBricks);
        }
        else
        {
            ResetBricks(bakedBricks);
        }
    }


    public GameObject[] GetBrickArray()
    {
        return bakedBricks.ToArray();
    }


    private BrickContainer LoadBricksFromFile()
    {

        return JsonUtility.FromJson<BrickContainer>(File.ReadAllText(Application.persistentDataPath + ConstClass.BrickJsonFileName));
    }

    private void CreateCubes(BrickContainer container)
    {
        foreach (Brick brickData in container.playbricks)
        {
            GameObject Brick = new GameObject();
            Brick.name = brickData.name;

            for (int i = 0; i < brickData.steps.Length; i++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

                cube.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                cube.transform.rotation = Quaternion.identity;

                cube.transform.position = Vector3.zero + brickData.steps[i];

                var cubeRenderer = cube.GetComponent<Renderer>();
                cubeRenderer.material.color = brickData.color;

                cube.transform.SetParent(Brick.transform);

                if (i == 0) { cube.name = "Pivot"; }
            }
            bakedBricks.Add(Brick);
        }
        
        
    }
    private  void SetBricksInCircle(List<GameObject> brickList)
    {
        Vector3 circleCenter = Vector3.zero;
        float radius = 5f;

        int brickCounter = 0;
        foreach (GameObject brick in brickList)
        {
            brickCounter++;
            brick.SetActive(true);

            // Calculate the round'about
            float angle = brickCounter * Mathf.PI * 2 / brickList.Count;
            float x = Mathf.Cos(angle) * radius;
            float z = Mathf.Sin(angle) * radius;

            // Get position from center
            Vector3 newPosition = circleCenter + new Vector3(x, 0, z);

            // Get "lookAt" rotation
            float angleDegrees = -angle * Mathf.Rad2Deg;
            Quaternion newRotation = Quaternion.Euler(0, angleDegrees, 0);

            // Set the bricks
            brick.transform.position = newPosition;
            brick.transform.rotation = newRotation;
        }
    }

    private void ResetBricks(List<GameObject> bricks)
    {
        foreach (GameObject brick in bakedBricks)
        {
            brick.transform.position = Vector3.zero;
            brick.SetActive(false);
        }
    }
}
