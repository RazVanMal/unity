﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ConstClass
{

    public const string BrickJsonFileName = "/brickdata.json";
    public const string ScoreJsonFileName = "/gamedata.json";

    public static readonly Vector3 X_Rotation = new Vector3(90f, 0f, 0f);
    public static readonly Vector3 Y_Rotation = new Vector3(0f, 90f, 0f);
    public static readonly Vector3 Z_Rotation = new Vector3(0f, 0f, 90f);

    public static readonly Vector3 Vector90 = new Vector3(90f,90f,90f);
    public static readonly Vector3 VectorHalf = new Vector3(0.5f, 0.5f, 0.5f);

    public const string Novice = "Novice";
    public const string Adventurer = "Adventurer";
    public const string Hero = "Hero";
}
