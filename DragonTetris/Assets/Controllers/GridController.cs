﻿
/* 
 * Conventions:
 *      Plane naming went by unity Vector3 conventions.
 *      (Zero, One, Forward, Back, Right, Left, Up, Down)
 * 
 * Vector3 list:
 *      |    0    |    1     |    2     |    3     |   4     |     5    |    6    |     7    |
 *      | (0,0,0) | (1,1,1)  | (0,0,1)  | (0,0,-1) | (1,0,0) | (-1,0,0) | (0,1,0) | (0,-1,0) |
 *      |   Zero  |   One    | Forward  |   Back   |  Right  |   Left   |   Up    |   Down   |
 * 
 * Concepts:
 *      Orthogonal space - where the plane "lives" as a 2d entity in a 3d world
 *      i.e : the "Left" plane "lives" on the (y,z) plane, and "moves" on the (-x) (X-axis, negative direction)
 *
 */

using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GridController : MonoBehaviour
{
    [SerializeField] List<GameObject> planes;
    [SerializeField] Vector3Int gridSize;
    [SerializeField] bool ShowOnEdit;

    [SerializeField] GameObject centerQuad;

    // The 3d game "board"
    public Transform[,,] gameBoardCube;

    private void Awake()
    {
        Debug.Log("Grid - Awake");

        gridSize = DataHandler.instance.gridSize;
        Debug.Log("    - gridSize set to "+gridSize.ToString());

        InitGameBoardCube();

        InitGrid();
        RefactorGrid();

        PlaceQuad();
    }

    private void PlaceQuad()
    {
        centerQuad.transform.position = new Vector3(gridSize.x/2, 0, -2);
    }

    #region Public Methods

    public Vector3Int GetGridSize()
    {
        return gridSize;
    }

    public void UpdateGameBoardCube(GameObject brick)
    {
        // Remove "Empty" game objects
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                for (int y = 0; y < gridSize.y; y++)
                {
                    if (gameBoardCube[x, y, z] != null)
                    {
                        if (gameBoardCube[x, y, z].parent == brick.transform)
                        {
                            gameBoardCube[x, y, z] = null;
                        }
                    }
                }
            }
        }

        // Fill the GameBoard with the relevant brick cubes
        foreach (Transform cube in brick.transform)
        {
            Vector3 pos = Utils.Round(cube.position);
            if (pos.y < gridSize.y)
            {
                gameBoardCube[(int)pos.x, (int)pos.y, (int)pos.z] = cube;
            }
        }
    }
    public Transform GetCubeAtPos(Vector3 pos)
    {
        if (pos.y > gridSize.y - 1) { return null; }
        else
        {
            return gameBoardCube[(int)pos.x, (int)pos.y, (int)pos.z];
        }
    }


    #endregion Public Methods

    #region InitGrid 

    private void InitGrid()
    {
        // Reset grid a cube of 1, at locaion 0
        foreach (GameObject plane in planes)
        {
            resetPlane(plane);
            rotatePlane(plane);
            movePlaneBy(plane, ConstClass.VectorHalf);
        }
    }

    private void resetPlane(GameObject plane)
    {
        plane.transform.position = Vector3.zero;
        plane.transform.rotation = Quaternion.identity;
        plane.transform.localScale = Vector3.one / 10;
    }

    private void rotatePlane(GameObject plane)
    {
        PlaneData planeData = plane.GetComponent<PlaneData>();

        Vector3 angle = Vector3.Scale(planeData.rotationAxis, ConstClass.Vector90);
        plane.transform.Rotate(angle.x, angle.y, angle.z);
    }

    private void movePlaneBy(GameObject plane, Vector3 deltaPos)
    {
        PlaneData planeData = plane.GetComponent<PlaneData>();
        Vector3 deltaMove = Vector3.Scale(planeData.moveDirection, deltaPos);
        plane.transform.position += deltaMove;
    }

    #endregion InitGrid

    #region RefactorGrid

    private void RefactorGrid()
    {
        // Resize grid according to gridSize
        foreach (GameObject plane in planes)
        {
            resizePlane(plane);
            centerGrid(plane);
            reTileGrid(plane);
        }
    }

    private void resizePlane(GameObject plane)
    {
        PlaneData planeData = plane.GetComponent<PlaneData>();
        Vector3 relevantSize = Vector3.Scale(planeData.orthoPlane, gridSize);

        Vector2 orthoPlane = Utils.translate_Vec3ToVec2(relevantSize);

        Vector3 newSize = new Vector3((float)orthoPlane.x / 10, 1, (float)orthoPlane.y / 10);
        plane.transform.localScale = newSize;
    }

    private void centerGrid(GameObject plane)
    {
        PlaneData planeData = plane.GetComponent<PlaneData>();
        Vector3 planeDirection = planeData.moveDirection + Vector3.one;
        Vector3 relativeCenter = (planeDirection / 2);

        plane.transform.position = new Vector3(
            transform.position.x + gridSize.x * relativeCenter.x,
            transform.position.y + gridSize.y * relativeCenter.y,
            transform.position.z + gridSize.z * relativeCenter.z);
    }

    private void reTileGrid(GameObject plane)
    {
        PlaneData planeData = plane.GetComponent<PlaneData>();
        Vector3 relevantSize = Vector3.Scale(planeData.orthoPlane, gridSize);

        plane.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = Utils.translate_Vec3ToVec2(relevantSize);
    }

    #endregion ResizeGrid

    #region Inspector - OnDraw

    private void OnDrawGizmosSelected()
    {
        if (!ShowOnEdit) { return; }
        foreach (GameObject plane in planes)
        {
            resetPlane(plane);            
            rotatePlane(plane);
            movePlaneBy(plane, ConstClass.VectorHalf);

            resizePlane(plane);
            centerGrid(plane);
            reTileGrid(plane);
        }
    }

    #endregion Inspector - OnDraw

    #region GameBoardCube

    private void InitGameBoardCube()
    {
        gameBoardCube = new Transform[gridSize.x, gridSize.y, gridSize.z];

        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                for (int z = 0; z < gridSize.z; z++)
                {
                    gameBoardCube[x, y, z] = null;
                }
            }
        }
    }


    #endregion GameBoardCube

}

