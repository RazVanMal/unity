﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataHandler : MonoBehaviour
{
    public static DataHandler instance;

    public Vector3Int gridSize;
    public int gameSpeed;
    public int difficulty;
    public int[] highScore = new int[] { 0, 0, 0, 0, 0 };
    public int score;

    
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);

        LoadData();

        Debug.Log("Data Awake");
        PrintData();
    }

    public void UpdateHighScore()
    {
        for (int i = highScore.Length-1; i >=0 ; i--)
        {
            if (score > highScore[i])
            {
                
                for (int j = 0; j<i; j++)
                {
                    highScore[j] = highScore[j+1];
                }
                highScore[i] = score;
            }
        }
        SaveData();
    }

    private void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + ConstClass.ScoreJsonFileName))
        {
            DataBlock temp = JsonUtility.FromJson<DataBlock>(File.ReadAllText(Application.persistentDataPath + ConstClass.ScoreJsonFileName));
          
            this.gridSize = temp.gridSize;
            this.gameSpeed = temp.gameSpeed;
            this.difficulty = temp.difficulty;
            this.highScore = temp.highScore;
            this.score = temp.score;
        }
        else
        {
            gridSize = new Vector3Int(5, 5, 5);
            gameSpeed = 1;
            highScore = new int[] { 1, 2, 3, 4, 5 };
            score = 0;
            SaveData();
        }
    }

    public void SaveData()
    {
       
        Debug.Log("Saving file");
        PrintData();

        string jsonSavePath = Application.persistentDataPath + ConstClass.ScoreJsonFileName;
        string jsonData = JsonUtility.ToJson(instance, true);
        File.WriteAllText(jsonSavePath, jsonData);
    }

    public void PrintData()
    {
        string arrString = "";
        foreach (var x in highScore) { arrString += "[" + x + "]"; }
        Debug.Log("    - gridSize   = " + gridSize.ToString());
        Debug.Log("    - gameSpeed  = " + gameSpeed.ToString());
        Debug.Log("    - difficulty = " + difficulty.ToString());
        Debug.Log("    - highScore  = " + arrString);
        Debug.Log("    - score      = " + score.ToString());
    }
}
