﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BrickToData : MonoBehaviour
{

    [Tooltip("Pivot at (0,0,0), cube scale at 0.9.")]
    [SerializeField] List<GameObject> handMadeBrickList;

    [Tooltip("Prints json to Debug.Log.")]
    [SerializeField] bool printoutFinishedJson = false;

    private BrickContainer container = new BrickContainer();
    private string jsonData = "";

    void Awake()
    {
        CreateBricks();
    }

    public void CreateBricks()
    {
        if (handMadeBrickList == null)
        {
            BricksCreatedIn_Code();
        }
        else
        {
            BricksCreatedIn_Editor();
        }

        WriteToFile();

        if (printoutFinishedJson)
        {
            PrintJson();
        }
    }

    private void BricksCreatedIn_Code()
    {
        Brick T_Brick = new Brick();
        T_Brick.name = "T_Brick";
        T_Brick.color = Color.red;
        T_Brick.steps = new[] { Vector3.zero, Vector3.forward, Vector3.back, Vector3.up };
        container.playbricks.Add(T_Brick);

        Brick C_Brick = new Brick();
        C_Brick.name = "C_Brick";
        C_Brick.color = Color.green;
        C_Brick.steps = new[] { Vector3.zero, Vector3.forward, Vector3.right, Vector3.up };
        container.playbricks.Add(C_Brick);

        Brick L_Brick = new Brick();
        L_Brick.name = "L_Brick";
        L_Brick.color = Color.blue;
        L_Brick.steps = new[] { Vector3.zero, Vector3.forward, (Vector3.forward * 2), Vector3.right };
        container.playbricks.Add(L_Brick);
    }
    
    private void BricksCreatedIn_Editor()
    {

        bool sampleColor = true;
        // Go thorugh all Bricks in the list
        foreach (GameObject handMadeBrick in handMadeBrickList)
        {
            Brick newBrick = new Brick();

            newBrick.name = handMadeBrick.name;
            newBrick.color = Color.black;
            newBrick.steps = new Vector3[handMadeBrick.transform.childCount];

            sampleColor = true; // smplae once per brick (Assume all cubes are the same color) (#TODO colorful cube?)
            int cubeCounter = 0;

            // Go thorugh all the Bricks' cubes
            foreach (Transform cube in handMadeBrick.transform)
            {
                // update each cubs' location (steps from Pivot)
                newBrick.steps[cubeCounter] = cube.transform.position;
                cubeCounter++;

                if (sampleColor)
                {
                    newBrick.color = cube.GetComponent<MeshRenderer>().sharedMaterial.color;
                    sampleColor = false;
                }
            }

            container.playbricks.Add(newBrick);
        }

    }

    private void WriteToFile()
    {
        string jsonSavePath = Application.persistentDataPath + ConstClass.BrickJsonFileName;
        jsonData = JsonUtility.ToJson(container, true);
        File.WriteAllText(jsonSavePath, jsonData);
    }

    private void PrintJson()
    {
        Debug.Log("---------------------------------");
        Debug.Log("json\n" + jsonData);
        Debug.Log("---------------------------------");
    }
}