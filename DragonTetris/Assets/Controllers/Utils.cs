﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static Vector2 translate_Vec3ToVec2(Vector3 targetVector)
    {
        Vector2 ortho2D = new Vector2();

        if (targetVector.x == 0)
        {
            ortho2D.x = targetVector.y;
            ortho2D.y = targetVector.z;
        }
        else if (targetVector.y == 0)
        {
            ortho2D.x = targetVector.x;
            ortho2D.y = targetVector.z;
        }
        else if (targetVector.z == 0)
        {
            ortho2D.x = targetVector.x;
            ortho2D.y = targetVector.y;
        }
        else { ortho2D = Vector2.zero; }

        return ortho2D;

    }

    public static Vector3 Round(Vector3 vec)
    {
        return new Vector3(
            Mathf.RoundToInt(vec.x),
            Mathf.RoundToInt(vec.y),
            Mathf.RoundToInt(vec.z));
    }


}
