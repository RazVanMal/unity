﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishIntro : MonoBehaviour
{

    [SerializeField] Animator anim;
    [SerializeField] float skipTime = 37f;

    void Awake()
    {
        AnimationClip clip;
        AnimationEvent evt = new AnimationEvent();
        
        evt.time = skipTime;
        evt.functionName = "EndScene";

        anim = GetComponent<Animator>();
        clip = anim.runtimeAnimatorController.animationClips[0];
        clip.AddEvent(evt);
    }

    // the function to be called as an event
    public void EndScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
