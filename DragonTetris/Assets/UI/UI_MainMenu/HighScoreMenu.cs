﻿using TMPro;
using UnityEngine;

public class HighScoreMenu : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI place_01;
    [SerializeField] TextMeshProUGUI place_02;
    [SerializeField] TextMeshProUGUI place_03;
    [SerializeField] TextMeshProUGUI place_04;
    [SerializeField] TextMeshProUGUI place_05;

    private void Start()
    {
        place_01.text = DataHandler.instance.highScore[0].ToString("D3");
        place_02.text = DataHandler.instance.highScore[1].ToString("D3");
        place_03.text = DataHandler.instance.highScore[2].ToString("D3");
        place_04.text = DataHandler.instance.highScore[3].ToString("D3");
        place_05.text = DataHandler.instance.highScore[4].ToString("D3");
    }

}
