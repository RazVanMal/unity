﻿using System;
using TMPro;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI speedValue;
    [SerializeField] TextMeshProUGUI difficultyValue;
    [SerializeField] TextMeshProUGUI gridSizeX;
    [SerializeField] TextMeshProUGUI gridSizeY;
    [SerializeField] TextMeshProUGUI gridSizeZ;


    private void Start()
    {
        speedValue.text = DataHandler.instance.gameSpeed.ToString("D2");
        gridSizeX.text = DataHandler.instance.gridSize.x.ToString("D2");
        gridSizeY.text = DataHandler.instance.gridSize.y.ToString("D2");
        gridSizeZ.text = DataHandler.instance.gridSize.z.ToString("D2");

        switch (DataHandler.instance.difficulty)
        {
            case 1: { difficultyValue.text = ConstClass.Novice; } break;
            case 2: { difficultyValue.text = ConstClass.Adventurer; } break;
            case 3: { difficultyValue.text = ConstClass.Hero; } break;
        }
    }

    #region public methods

    public void SetGameSpeed(int amount)
    {
        DataHandler.instance.gameSpeed += amount;
        DataHandler.instance.gameSpeed = clampVal(DataHandler.instance.gameSpeed, 1,10);

        speedValue.text = DataHandler.instance.gameSpeed.ToString("D2");
    }

    public void SetDifficulty(int amount)
    {
        DataHandler.instance.difficulty += amount;
        DataHandler.instance.difficulty = clampVal(DataHandler.instance.difficulty, 1, 3);

        switch (DataHandler.instance.difficulty)
        {
            case 1: { difficultyValue.text = ConstClass.Novice; } break;
            case 2: { difficultyValue.text = ConstClass.Adventurer; } break;
            case 3: { difficultyValue.text = ConstClass.Hero; } break;
        }
    }

    public void SetGridSizeX(int amount)
    {
        DataHandler.instance.gridSize.x += amount;
        DataHandler.instance.gridSize.x = clampVal(DataHandler.instance.gridSize.x, 5, 20);
        gridSizeX.text = DataHandler.instance.gridSize.x.ToString("D2");
    }

    public void SetGridSizeY(int amount)
    {
        DataHandler.instance.gridSize.y += amount;
        DataHandler.instance.gridSize.y = clampVal(DataHandler.instance.gridSize.y, 5, 20);
        gridSizeY.text = DataHandler.instance.gridSize.y.ToString("D2");
    }

    public void SetGridSizeZ(int amount)
    {
        DataHandler.instance.gridSize.z += amount;
        DataHandler.instance.gridSize.z = clampVal(DataHandler.instance.gridSize.z, 5, 20);
        gridSizeZ.text = DataHandler.instance.gridSize.z.ToString("D2");
    }

    private int clampVal(int val, int min, int max)
    {
        if (val >= (max+1)) { val = max; }
        if (val <= (min-1)) { val = min; }
        return val;
    }

    #endregion public methods
}
