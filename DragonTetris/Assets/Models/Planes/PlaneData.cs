﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneData : MonoBehaviour
{
    [Tooltip("On which axis it moves")]
    public Vector3 moveDirection;

    [Tooltip("Preffered rotation Axis")]
    public Vector3 rotationAxis;

    [Tooltip("Put 1 in relevant axis")]
    public Vector3 orthoPlane;
}
