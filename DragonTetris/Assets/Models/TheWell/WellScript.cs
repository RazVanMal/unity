﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WellScript : MonoBehaviour
{
    GameObject[] wellWalls;
    [SerializeField] int numberOfWalls;

    [Tooltip("Factor to multiply longest grid axis length")]
    [SerializeField] float radiusFactor = 5f;//1.5f;

    [SerializeField] Material wellMaterial;

    private void Start()
    {
        Vector3Int gridSize = DataHandler.instance.gridSize;
        Vector3 gridCenter = Vector3.Scale(gridSize, ConstClass.VectorHalf);
        float wellRadius = Mathf.Max(gridSize.x, gridSize.z) * radiusFactor;

        wellWalls = new GameObject[numberOfWalls];

        for (int i = 0; i < wellWalls.Length; i++)
        {
            GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
            quad.name = "Wall_" + i;
            float partialSize = wellRadius * Mathf.PI * 2 / wellWalls.Length;
            quad.transform.localScale = new Vector3(partialSize+1, gridSize.y*2, partialSize+1);

            quad.GetComponent<Renderer>().material = wellMaterial;

            quad.transform.parent = this.transform;
            

            wellWalls[i] = quad;
        }

        DrawWellWals(wellWalls, gridCenter, wellRadius);

        GameObject floor = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        floor.name = "Floor";

        Vector3 centerGrid = new Vector3(gridSize.x/2,1,gridSize.z /2);
        floor.transform.position = centerGrid;
        floor.transform.localScale = new Vector3(gridSize.x * radiusFactor * 2.5f, 0.01f,gridSize.z * radiusFactor * 2.5f);
        floor.GetComponent<Renderer>().material = wellMaterial;
        floor.transform.parent = this.transform;

        floor.transform.position += Vector3.down *  gridSize.y/2;
        floor.transform.position += Vector3.up;
    }



    private void DrawWellWals(GameObject[] wellWalls, Vector3 circleCenter, float wallRadius)
    {

        int wallCounter= 0;
        foreach (GameObject brick in wellWalls)
        {
            wallCounter++;
            brick.SetActive(true);

            // Calculate the round'about
            float angle = wallCounter * Mathf.PI * 2 / wellWalls.Length;
            float x = Mathf.Cos(angle) * wallRadius;
            float z = Mathf.Sin(angle) * wallRadius;

            // Get position from center
            Vector3 newPosition = circleCenter + new Vector3(x, 0, z);

            // Get "lookAt" rotation
            float angleDegrees = -angle * Mathf.Rad2Deg;
            Quaternion newRotation = Quaternion.Euler(0, angleDegrees+90f, 0);

            // Set the bricks
            brick.transform.position = newPosition;
            brick.transform.rotation = newRotation;
        }
    }

}
