﻿using System;
using UnityEngine;

[Serializable]
public class DataBlock
{
    public Vector3Int gridSize;
    public int gameSpeed;
    public int difficulty;
    public int[] highScore;
    public int score;
}