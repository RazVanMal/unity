﻿
using System;
using System.Collections.Generic;

[Serializable]
public class BrickContainer 
{
    public List<Brick> playbricks = new List<Brick>();
}
