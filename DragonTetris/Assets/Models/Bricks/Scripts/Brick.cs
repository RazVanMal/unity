﻿using System;
using UnityEngine;

[Serializable]
public class Brick 
{
    public string name;
    public Color color;
    public Vector3[] steps;
}